package ca.qc.claurendeau.storage;

public class Buffer
{
    // VOTRE CODE

    public Buffer(int capacity)
    {
        // VOTRE CODE
    }

    // returns the content of the buffer in form of a string
    public String toString()
    {
        // VOTRE CODE
        String string = null;
        return string;
    }

    // returns the capacity of the buffer
    public int capacity()
    {
        // VOTRE CODE
        return -1;
    }

    // returns the number of elements currently in the buffer
    public int getCurrentLoad()
    {
        // VOTRE CODE
        return -1;
    }

    // returns true if buffer is empty, false otherwise
    public boolean isEmpty()
    {
        // VOTRE CODE
        return false;
    }

    // returns true if buffer is full, false otherwise
    public boolean isFull()
    {
        // VOTRE CODE
        return true;
    }

    // adds an element to the buffer
    // Throws an exception if the buffer is full
    public synchronized void addElement(Element element) // throws BufferFullException
    {
        // VOTRE CODE
    }
    
    // removes an element and returns it
    // Throws an exception if the buffer is empty
    public synchronized Element removeElement() // throws BufferEmptyException
    {
        // VOTRE CODE
        return new Element();
    }
}
